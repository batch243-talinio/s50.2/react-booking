
//pages import
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Page404 from './pages/NotFound';
import CourseView from './pages/CourseView';

import {useState, useEffect} from 'react';
import{BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import './App.css';


import {UserProvider} from './UserContext';

function App() {

//State Hook for the user that will globally accessible using the useContaxt
//This will also be used to storethe user info and be used for validating if a user is logged in on the app or not

  const [user, setUser] = useState({id: null, isAdmin:false});

  //function for clearing localstorage
  const unSetUser = () => {
    localStorage.clear();
  }

  useEffect(()=> {
    fetch(`${process.env.REACT_APP_URI}/users/profile`,{
        headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}})
			.then(response=> response.json())
			.then(data => {
				console.log(data);
				
				setUser({id: data._id, isAdmin: data.isAdmin})
			})
  }, [])

  return (
    /*Router/BrowserRouter > Routes > Route*/

    <UserProvider value = {{user, setUser, unSetUser}}>
      <Router>
        <AppNavbar/>
          <Routes>
            <Route path = "/" element = {<Home/>}/>
            <Route path = "/courses" element = {<Courses/>}/>
            <Route path = "/courses/:courseId" element = {<CourseView/>}/>
            <Route path = "/login" element = {<Login/>}/>
            <Route path = "/register" element = {<Register/>} />
            <Route path = "/logout" element = {<Logout/>} />
            <Route path = "*" element = {<Page404/>} />
          </Routes>
      </Router>
    </UserProvider>
    
      
  );
}

export default App;
